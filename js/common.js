$(function () {
  var __DEV__ = false

  if (__DEV__) {
    window.rooturl = 'http://120.79.61.69:9111/api/v2'
  } else {
    window.rooturl = 'https://fhs.symboltree.cn/api/v2'
  }

  $(window).scroll(function () {
    var distop = $(window).scrollTop()
    if (distop > 300) {
      $('.backtop-btn').css('opacity', 1)
      $(".headfixed").addClass("isshadow")
    } else {
      $('.backtop-btn').css('opacity', 0)
      $(".headfixed").removeClass("isshadow")
    }
  })

  var app = new Vue({
    el: '#app',
    data: {
      courseList: [], ///全部课程
      precourse: [], ///预习课
      synccourse: [], ///同步课
      expcourse: [], ///专题课
      userName: "",
      userPhone: "",
      sex: 0,
      remark: ""
    },
    methods: {
      getCourse: function () {
        var that = this
        $.ajax({
          method: 'GET',
          url: `${rooturl}/courses/wx/list`,
          dataType: 'json',
          async: true,
          contentType: 'application/json',
        }).done(function (msg) {
          if (msg.statusCode == 200) {
            that.courseList = msg.data.courses
            for (var i = 0; i < that.courseList.length; i++) {
              that.courseList[i].className = "";
              if (i % 3 === 0) {
                that.courseList[i].className = "card-left";
              } else if (i % 3 === 1) {
                that.courseList[i].className = "card-center";
              } else if (i % 3 === 2) {
                that.courseList[i].className = "card-right";
              }
              that.courseList[i].image = "./images/course/" + that.courseList[i].id + ".jpg";
            }
            // console.log(that.courseList)
            if (msg.data.courses && msg.data.courses.length > 0) {
              msg.data.courses.map(function (e) {
                if (e.type === 1 || e.type === 2) {
                  that.precourse.push(e)
                } else if (e.type === 3) {
                  that.synccourse.push(e)
                } else if (e.type === 4) {
                  that.expcourse.push(e)
                }
              })
            }
          }
        });

      },
      submitUserInfo: function () {
        var phoneReg = /^(0|86|17951)?(13[0-9]|15[012356789]|166|17[3678]|18[0-9]|14[57])[0-9]{8}$/
        var nameReg = /(^[\u4e00-\u9fa5]{1}[\u4e00-\u9fa5\.·。]{0,8}[\u4e00-\u9fa5]{1}$)|(^[a-zA-Z]{1}[a-zA-Z\s]{0,8}[a-zA-Z]{1}$)/
        if (!nameReg.test(this.userName)) {
          alert('姓名填写有误')
          return false
        }
        if (!phoneReg.test(this.userPhone)) {
          alert('手机号填写有误')
          return false
        }
        var data = {
          "userName": this.userName,
          "phone": this.userPhone,
          "sex": this.sex,
          "remark": this.remark
        };

        $.ajax({
          method: 'POST',
          url: `${rooturl}/anonymous/free-expr`,
          data: JSON.stringify(data),
          dataType: 'json',
          async: true,
          contentType: 'application/json',
        }).done(function (msg) {
          if (msg.statusCode == 200) {
            $('#comments').modal('close');
            $('#modal-confirm').modal();
          } else {
            alert(msg.message);
          }
        });
      }
    }

  })

  app.getCourse()
})

var phoneReg = /^[1][3,4,5,6,7,8,9][0-9]{9}$/;
var nameReg = /[^\a-zA-Z\u4E00-\u9FA5]/;

function bindInputOnChange() {
  var name = $("#userName").val();
  var phone = $("#phone").val();

  if (!isEmpty(name) && !isEmpty(phone) && phone.length === 11) {
    $("#submitBtn").attr("data-am-modal", "{target: '#modal-confirm', width: 400, height: 250}");
  } else {
    $("#submitBtn").attr("data-am-modal", "");
  }
}

function isEmpty(value) {
  if (value === undefined || value === null || value.trim() === "") {
    return true;
  }
  return false
}

window.onload = function() {
  var bodyDom = document.getElementById('body-main')
  var imgDom = document.getElementById('img-main')
  if (imgDom) {
    var scale = imgDom.naturalHeight / imgDom.naturalWidth //原始宽高比
    initHeight()
    window.addEventListener('resize', function (e) {
      initHeight()
    })
  }

  function initHeight() {
    bodyDom.style.height = bodyDom.clientWidth * scale + 'px'
  }
}
